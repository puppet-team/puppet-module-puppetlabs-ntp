puppet-module-puppetlabs-ntp (7.2.0-2) UNRELEASED; urgency=medium

  * Bump debhelper from old 11 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Replace use of deprecated $ADTTMP with $AUTOPKGTEST_TMP.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 01 Sep 2022 14:29:44 -0000

puppet-module-puppetlabs-ntp (7.2.0-1) unstable; urgency=medium

  * New upstream version 7.2.0
  * d/tests: Replace embedded sharness copy by the package,
             Thanks Lars (Closes: #906631)
  * d/control: Bump to Standards-Version 4.2.1 (no changes needed)

 -- Sebastien Badia <sbadia@debian.org>  Sun, 02 Sep 2018 14:13:42 +0200

puppet-module-puppetlabs-ntp (7.1.1-1) unstable; urgency=medium

  [ Thomas Bechtold ]
  * Remove myself from Uploaders (Closes: #892666)

  [ Sebastien Badia ]
  * New upstream version 7.1.1
  * d/watch: Bump to version 4 and use HTTPS for URI
  * d/upstream: Added Upstream metadata
  * d/control:
    + Bump to Standards-Version 4.1.3 (no changes needed)
    + Use salsa.debian.org in Vcs-* fields
    + Added myself as Uploader
  * d/copyright: Update copyritght years

 -- Sebastien Badia <sbadia@debian.org>  Thu, 22 Mar 2018 20:09:09 +0100

puppet-module-puppetlabs-ntp (7.0.0-1) unstable; urgency=medium

  * Team upload.
  * Imported upstream release 7.0.0.
  * Depend on puppet, not puppet-common, and bump minimum version to 4.9.4
    following new upstream requirements.
  * Install new Japanese README file.
  * Include NOTICE and MAINTAINERS.md in the installed docs.
  * Set Rules-Requires-Root: no.
  * Run wrap-and-sort -ast.
  * Remove myself from Uploaders.
  * Update debhelper compatibility level to V11.
  * Update standards version to 4.1.2 (no changes required).

 -- Russ Allbery <rra@debian.org>  Wed, 20 Dec 2017 21:23:39 -0800

puppet-module-puppetlabs-ntp (6.0.0-2) unstable; urgency=medium

  * Also install the new data and types directories and hiera.yaml.
  * Fix debian/copyright ordering (noticed by Lintian).
  * Add myself to Uploaders.

 -- Russ Allbery <rra@debian.org>  Thu, 24 Nov 2016 10:39:12 -0800

puppet-module-puppetlabs-ntp (6.0.0-1) unstable; urgency=medium

  * Imported upstream release 6.0.0
  * debian/copyright: Use https URL
  * Use debhelper 10
  * Add versioned dependency on puppet-module-puppetlabs-stdlib

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sat, 19 Nov 2016 18:35:34 +0100

puppet-module-puppetlabs-ntp (4.2.0-2) unstable; urgency=medium

  * Update dependencies for puppet 4

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Tue, 24 May 2016 09:54:38 +0200

puppet-module-puppetlabs-ntp (4.2.0-1) unstable; urgency=medium

  * Imported upstream relase 4.2.0
  * Declare compliance with Debian policy 3.9.8
  * Update VCS-* URLs
  * Add myself to Uploaders list
  * Update source homepage

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Wed, 18 May 2016 21:11:24 +0200

puppet-module-puppetlabs-ntp (4.1.1-1) unstable; urgency=medium

  * Team upload.
  * Imported upstream release 4.1.1

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Wed, 25 Nov 2015 12:37:24 +0100

puppet-module-puppetlabs-ntp (4.1.0-1) unstable; urgency=medium

  * Imported upstream release 4.1.0
  * [75f8e2e] Add DEP-8 tests

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sat, 12 Sep 2015 00:30:18 +0200

puppet-module-puppetlabs-ntp (4.0.0-1) unstable; urgency=medium

  * Imported upstream relase 4.0.0

 -- Michael Weiser <michael.weiser@gmx.de>  Tue, 23 Jun 2015 15:31:25 +0200

puppet-module-puppetlabs-ntp (3.3.0-1) unstable; urgency=medium

  * Imported upstream relase 3.3.0

 -- Michael Weiser <michael.weiser@gmx.de>  Fri, 21 Nov 2014 12:29:17 +0100

puppet-module-puppetlabs-ntp (3.2.1-1) unstable; urgency=medium

  * Imported upstream relase 3.2.1
  * Declare compliance with Debian policy 3.9.6
  * Update debian/control metadata with cme (Vcs-*)
  * Update dependencies
  * Update file lists

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Fri, 24 Oct 2014 22:14:54 +0200

puppet-module-puppetlabs-ntp (3.0.3-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Bechtold <toabctl@debian.org>  Fri, 07 Mar 2014 21:34:12 +0100

puppet-module-puppetlabs-ntp (3.0.1-2) unstable; urgency=medium

  * debian/watch: use redirector from qa.debian.org.

 -- Thomas Bechtold <toabctl@debian.org>  Wed, 22 Jan 2014 07:54:56 +0100

puppet-module-puppetlabs-ntp (3.0.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Use Debian address in Uploaders field.
    - Bump Standards-Version to 3.9.5. No further changes.

 -- Thomas Bechtold <toabctl@debian.org>  Sun, 05 Jan 2014 17:41:08 +0100

puppet-module-puppetlabs-ntp (2.0.1-1) unstable; urgency=low

  [ Thomas Bechtold ]
  * Initial release (Closes: #720094)

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sun, 08 Sep 2013 19:20:50 +0200
